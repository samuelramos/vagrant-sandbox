#!/bin/bash

yum install -y bc

yum install -y gtk2

echo 'kernel.shmmni = 4096' >> /etc/sysctl.conf
echo 'kernel.shmall = 2097152' >> /etc/sysctl.conf
echo 'kernel.shmmax = 268435456' >> /etc/sysctl.conf
echo 'kernel.sem = 32 4096 32 128' >> /etc/sysctl.conf

echo 'fs.file-max = 524288'  >> /etc/sysctl.conf

echo "net.ipv4.tcp_fin_timeout = 30" >> /etc/sysctl.conf
echo "net.ipv4.tcp_keepalive_time = 300" >> /etc/sysctl.conf
echo "net.ipv4.tcp_keepalive_intvl = 15" >> /etc/sysctl.conf
echo "net.ipv4.tcp_keepalive_probes = 5" >> /etc/sysctl.conf

echo 'mqm       hard  nofile     10240'  >> /etc/security/limits.conf
echo 'mqm       soft  nofile     10240'  >> /etc/security/limits.conf

echo 'mqm       hard  nproc     4096'  >> /etc/security/limits.conf
echo 'mqm       soft  nproc     4096'  >> /etc/security/limits.conf


sysctl -p

ulimit -n

#su mqm -c "/opt/IBM/mqm/bin/mqconfig"
