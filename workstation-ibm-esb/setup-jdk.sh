#!/bin/bash

JDK_VERSION=$1

yum install -y $JDK_VERSION

java -version

echo 'export JAVA_HOME=$(readlink -f /usr/bin/java | sed "s:/jre/bin/java::")' > /etc/profile.d/java_env.sh
echo 'export PATH=$JAVA_HOME:$PATH' >>  /etc/profile.d/java_env.sh

source /etc/profile

echo $JAVA_HOME
