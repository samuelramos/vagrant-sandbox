#!/bin/bash

PROXY_CERTIFICATE=$1

\cp /vagrant/$PROXY_CERTIFICATE /etc/pki/ca-trust/source/anchors/root-CA.crt
update-ca-trust extract
