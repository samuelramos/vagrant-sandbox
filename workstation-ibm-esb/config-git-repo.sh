#!/bin/bash

SSH_KEY_PUBLIC=$1
SSH_KEY_PRIVATE=$2

#dummy generation
ssh-keygen -f /home/vagrant/.ssh/$SSH_KEY_PRIVATE -N ""

echo $'Host bitbucket.org\n\tStrictHostKeyChecking no' > /home/vagrant/.ssh/config

chown vagrant /home/vagrant/.ssh/config
chmod 600 /home/vagrant/.ssh/config

\cp /vagrant/$SSH_KEY_PRIVATE /home/vagrant/.ssh/
chown vagrant /home/vagrant/.ssh/$SSH_KEY_PRIVATE
chmod 600 /home/vagrant/.ssh/$SSH_KEY_PRIVATE

\cp /vagrant/$SSH_KEY_PUBLIC /home/vagrant/.ssh/
chown vagrant /home/vagrant/.ssh/$SSH_KEY_PUBLIC
chmod 600 /home/vagrant/.ssh/$SSH_KEY_PUBLIC

eval `ssh-agent`

ssh-add /home/vagrant/.ssh/$SSH_KEY_PRIVATE

mkdir /home/vagrant/git-repo/
