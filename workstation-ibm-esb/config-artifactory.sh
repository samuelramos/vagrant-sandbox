#!/bin/bash

#if artifactory server as a ssl certificate it should be necessary to add it to the java keystore
#keytool -import -noprompt -trustcacerts -alias <Artifactory-CRT-alias> -file <Artifactory-CRT-cer> -keystore $JAVA_HOME/jre/lib/security/cacerts -storepass changeit
