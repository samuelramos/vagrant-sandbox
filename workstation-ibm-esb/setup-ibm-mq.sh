#!/bin/bash

IBM_MQ_VERSION="mqm-$1"
IBM_MQ_PACKAGE=$2
IBM_MQ_PACKAGE_LOCATION=$3


if [ -d "/opt/IBM/$IBM_MQ_VERSION" ]; then
  exit
fi


#pre-requirements

cd $IBM_MQ_PACKAGE_LOCATION

mkdir /home/vagrant/$IBM_MQ_VERSION

tar -zxvf $IBM_MQ_PACKAGE -C /home/vagrant/$IBM_MQ_VERSION

#setup

cd /home/vagrant/$IBM_MQ_VERSION/MQServer

./mqlicense.sh -accept

rpm --prefix /opt/IBM/$IBM_MQ_VERSION -ivh MQSeriesExplorer-*.rpm MQSeriesRuntime-*.rpm MQSeriesJRE-*.rpm MQSeriesJava-*.rpm MQSeriesServer-*.rpm MQSeriesFTBase-*.rpm MQSeriesFTAgent-*.rpm MQSeriesFTService-*.rpm MQSeriesFTLogger-*.rpm MQSeriesFTTools-*.rpm MQSeriesAMS-*.rpm MQSeriesGSKit-*.rpm MQSeriesClient-*.rpm MQSeriesMan-*.rpm MQSeriesSDK-*.rpm

#create the symbolic link
ln -s /opt/IBM/$IBM_MQ_VERSION /opt/IBM/mqm

#echo 'export MQ_INSTALLATION_PATH=/opt/IBM/$IBM_MQ_VERSION' >  /etc/profile.d/mq_env.sh

echo 'export MQ_INSTALLATION_PATH=/opt/IBM/mqm' >  /etc/profile.d/mq_env.sh

source /etc/profile

$MQ_INSTALLATION_PATH/bin/setmqinst -i -p $MQ_INSTALLATION_PATH

$MQ_INSTALLATION_PATH/bin/crtmqenv -n Installation1

. $MQ_INSTALLATION_PATH/bin/setmqenv -s

dspmqver

usermod --password $(openssl passwd -1 admin) mqm

usermod -a -G mqm vagrant

#exho 'vagrant ALL=NOPASSWD: /bin/su - mqm' > /etc/sudoers

#cp /etc/skel/.bash_profile ~/.bash_profile
cp /etc/skel/.bash_profile /var/mqm/.bash_profile
chown mqm /var/mqm/.bash_profile

#cp /etc/skel/.bashrc ~/.bashrc
cp /etc/skel/.bashrc /var/mqm/.bashrc
chown mqm /var/mqm/.bashrc

rm -rf /home/vagrant/$IBM_MQ_VERSION
