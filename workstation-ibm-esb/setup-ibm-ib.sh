#!/bin/bash

IBM_IB_VERSION="iib-$1"
IBM_IB_PACKAGE=$2
IBM_IB_PACKAGE_LOCATION=$3

if [ -d "/opt/IBM/$IBM_IB_VERSION" ]; then
  exit
fi

#pre-requirements

cd $IBM_IB_PACKAGE_LOCATION

mkdir /home/vagrant/$IBM_IB_VERSION

tar -zxvf $IBM_IB_PACKAGE -C /home/vagrant/$IBM_IB_VERSION

#setup

cd /home/vagrant/$IBM_IB_VERSION

mkdir -p /opt/IBM

mv $IBM_IB_VERSION /opt/IBM/

#create the symbolic link
ln -s /opt/IBM/$IBM_IB_VERSION /opt/IBM/iib

#echo 'export IIB_INSTALLATION_PATH=/opt/IBM/$IBM_IB_VERSION' > /etc/profile.d/iib_env.sh
#echo 'export PATH=$IIB_INSTALLATION_PATH:$PATH' >>  /etc/profile.d/iib_env.sh

echo 'export IIB_INSTALLATION_PATH=/opt/IBM/iib' > /etc/profile.d/iib_env.sh
echo 'export PATH=$IIB_INSTALLATION_PATH:$PATH' >>  /etc/profile.d/iib_env.sh

source /etc/profile

cd $IIB_INSTALLATION_PATH


./iib make registry global accept license silently

locale

echo 'export LC_ALL=C' > /etc/profile.d/iib_lang_override.sh

source /etc/profile

locale

usermod -a -G mqbrkrs mqm
usermod -a -G mqbrkrs vagrant

./iib verify all

./iib version


rm -rf /home/vagrant/$IBM_IB_VERSION


#\cp /vagrant/iib-toolkit.desktop /usr/share/applications/

echo '[Desktop Entry]' > /usr/share/applications/iib-toolkit.desktop
echo 'Version=1.0' >> /usr/share/applications/iib-toolkit.desktop
echo 'Type=Application' >> /usr/share/applications/iib-toolkit.desktop
echo 'Encoding=UTF-8' >> /usr/share/applications/iib-toolkit.desktop
echo 'Exec=/opt/IBM/iib/iib toolkit' >> /usr/share/applications/iib-toolkit.desktop
echo 'Icon=/opt/IBM/mqm/mqexplorer/eclipse/icon.xpm' >> /usr/share/applications/iib-toolkit.desktop
echo 'Name=IBM Integration Toolkit' >> /usr/share/applications/iib-toolkit.desktop
echo 'Comment=IBM Integration Toolkit' >> /usr/share/applications/iib-toolkit.desktop
echo 'Categories=Development' >> /usr/share/applications/iib-toolkit.desktop
