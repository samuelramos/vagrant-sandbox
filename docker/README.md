vagrant-docker
==============

## Pre-Requirements
-------------------

Before start you should have the following software installed in your workstation

- Vagrant 2.2.4
[download](https://www.vagrantup.com/downloads.html)
- VirtualBox 6.0.4
[download](https://www.virtualbox.org/wiki/Downloads)
- VirtualBox 6.0.4 Oracle VM VirtualBox Extension Pack
[download](https://www.virtualbox.org/wiki/Downloads)


Initiate your vagrant-docker environment
----------------------------------------

### 1) Initialization of the environment variables

If you're behind a enterprise proxy should be necessary to configure the proxy address

#### If you're using a mac

Open a new Terminal and run the following commands

```console
$ set HTTP_PROXY=http://address:port/
```
and
```console
$ set HTTPS_PROXY=https://address:port/
```
and
```console
$ set HOME=\Users\%USER%
```

#### If you're using a PC

Open a new Command Prompt and run the following commands

```console
# set HTTP_PROXY=http://address:port/
```
and
```console
# set HTTPS_PROXY=http://address:port/
```
and
```console
#set HOMEPATH=\Users\%USERNAME%
```

### 2) Config functionalities

You should modify the config.yaml file accordingly the environment you want to create in your local machine

### 3) Configure a enterprise proxy certificate in Vagrant

If you're behind a enterprise proxy it should be necessary to configure the ssl certificate in vagrant

#### If you're using a mac

1. Go to your Vagrant home directory (e.g. '/opt/vagrant/')
2. Enter into 'embedded' directory
3. Open and/or edit 'cacert.pem' as administrator
4. Open your enterprise proxy certificate, copy it's content and add it to the end of 'cacert.pem' file

#### If you're using a PC

1. Go to your Vagrant home directory (e.g. 'C:\HashiCorp\Vagrant\')
2. Enter into 'embedded' directory
3. Open and/or edit 'cacert.pem' as administrator
4. Open your enterprise proxy certificate, copy it's content and add it to the end of 'cacert.pem' file

### 4) Initialization and startup of the vagrant-docker

In previous Terminal or Command Prompt (from step #1), navigate to the vagrant-docker git directory and run the following command

```console
$ vagrant up
```

It will take a few minutes to complete, but, once the command finishes the VM is ready to use.

### 5) Using vagrant-docker

1. To stop the vagrant-docker you can use the operative system functionalities as usual, or use the VirtualBox
2. To start the vagrant-docker again, you can use the VirtualBox


Destroy your vagrant-docker environment
---------------------------------------

You can destroy the vagrant-docker through VirtualBox or running the following command on the Terminal or Command Prompt you use to start it (from step #4)

```console
$ vagrant destroy
```

## Restore your vagrant-docker environment
------------------------------------------

1. Backup all your work that can be subject of loss and then
2. Destroy your vagrant-docker environment
3. Initiate your vagrant-docker environment

## Update your vagrant-docker environment
-----------------------------------------

1. Backup all your work that can be subject of loss and then
2. Pull the latest release from vagrant-docker repository
3. Destroy your vagrant-docker environment
4. Initiate your vagrant-docker environment

## Know issues
--------------
