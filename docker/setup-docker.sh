#!/bin/bash

yum install -y yum-utils device-mapper-persistent-data lvm2

sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

yum install -y docker-ce docker-ce-cli containerd.io

#enable Docker to start on boot
systemctl enable docker

#start Docker
systemctl start docker

mkdir /etc/systemd/system/docker.service.d/
touch /etc/systemd/system/docker.service.d/docker.conf

#enable Docker remote API - expose Docker’s TCP port
echo "[Service]
ExecStart=
ExecStart=/usr/bin/dockerd -H tcp://0.0.0.0:4243 -H unix:///var/run/docker.sock" > /etc/systemd/system/docker.service.d/docker.conf

#restart the services
systemctl daemon-reload
systemctl restart docker

usermod -a -G docker vagrant
