vagrant-getting-started
=======================


##1) Init the vagrant environment

`$ vagrant init`

```
A `Vagrantfile` has been placed in this directory. You are now ready to `vagrant up` your first virtual environment! Please read the comments in the Vagrantfile as well as documentation on `vagrantup.com` for more information on using Vagrant.
```

##2) Edit the vagrant file

```
[...]
config.vm.box = "centos/7"

[...]
config.vm.network :forwarded_port, guest: 80, host: 4567
[...]
```

##3) Start the VM

`$ vagrant up`

##4) Test it! Open the browser using the following URL

(http://localhost:4567/)

##5) Enter into the running VM using ssh

```
$ vagrant ssh
[vagrant@localhost ~]$
```

##6) Exit the VM

```
$ exit
logout
Connection to 127.0.0.1 closed.
```

##6) Check VM status, stop it and destroy it

```
$ vagrant status
Current machine states:

default                   running (virtualbox)

The VM is running. To stop this VM, you can run `vagrant halt` to shut it down forcefully, or you can run `vagrant suspend` to simply suspend the virtual machine. In either case, to restart it again, simply run `vagrant up`.
$ vagrant destroy
default: Are you sure you want to destroy the 'default' VM? [y/N] y
==> default: Forcing shutdown of VM...
==> default: Destroying VM and associated drives...
```
