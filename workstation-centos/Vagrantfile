# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'yaml'

#required plugins
#vagrant-proxyconf
#vagrant-reload
#vagrant-vbguest
#list required plugins
#vagrant-proxyconf (2.0.1, global)
#vagrant-reload (0.0.1, global)
#vagrant-vbguest (0.17.2, global)
#remove plugins
#vagrant plugin uninstall <name>
#install plugins
#vagrant plugin install <name>
#vagrant plugin install <name> --plugin-version <version>

required_plugins = %w(vagrant-proxyconf vagrant-reload vagrant-vbguest)

plugins_to_install = required_plugins.select { |plugin| not Vagrant.has_plugin? plugin }
if not plugins_to_install.empty?
  puts "Installing plugins: #{plugins_to_install.join(' ')}"
  if system "vagrant plugin install #{plugins_to_install.join(' ')}"
    exec "vagrant #{ARGV.join(' ')}"
  else
    abort "Installation of one or more plugins has failed. Aborting."
  end
end

#load configuration from config.yaml

configuration = YAML.load_file('config.yaml')
puts "Configuration: #{configuration.inspect}\n\n"

#base configuration

$vm_name=configuration.fetch('vm').fetch('name')
$vm_memory=configuration.fetch('vm').fetch('memory')
$vm_cpus=configuration.fetch('vm').fetch('cpus')
$vm_ip=configuration.fetch('vm').fetch('ip')

if configuration.fetch('vm').has_key?('gui')
  $vm_gui = configuration.fetch('vm').fetch('gui')
  #by parameter at 'vagrant up'
  #$'gui='false' vagrant up
  #$vm_gui = ENV['gui']
else
  $vm_gui = false
end

if configuration.fetch('vm').has_key?('proxy')
  $vm_proxy=true
  $vm_proxy_http=configuration.fetch('vm').fetch('proxy').fetch('proxy_http')
  $vm_proxy_https=configuration.fetch('vm').fetch('proxy').fetch('proxy_https')
  $vm_no_proxy=configuration.fetch('vm').fetch('proxy').fetch('no_proxy')
  if configuration.fetch('vm').fetch('proxy').has_key?('certificate')
    $vm_proxy_certificate=configuration.fetch('vm').fetch('proxy').fetch('certificate')
  else
    $vm_proxy_certificate=''
  end
else
  $vm_proxy=false
end

$host_shared_folder=configuration.fetch('host').fetch('shared_folder')
$vm_shared_folder="/shared/host"

#addons configuration

#[...]

#other configuration

#JDK
$jdk_version="java-1.8.0-openjdk-devel"


Vagrant.configure("2") do |config|

  #if behind a enterprise proxy should be necessary to configure the proxy address and exclusions
  #requires the installation of vagrant-proxyconf
  if $vm_proxy == true
    config.proxy.http = $vm_proxy_http
    config.proxy.https = $vm_proxy_https
    config.proxy.no_proxy = $vm_no_proxy
  end

  #vm initialization
  config.vm.box = "centos/7"
  config.vm.network "private_network", ip: $vm_ip

  #shared folders
  #rsync is slower than virtualbox
  #virtualbox requires the vagrant-vbguest plugin

  #config.vm.synced_folder $host_shared_folder, $vm_shared_folder, type: "rsync"

  if $host_shared_folder != ''
    config.vm.synced_folder $host_shared_folder, $vm_shared_folder, type: "virtualbox"
  end

  #vm specs
  config.vm.provider "virtualbox" do |vb|
     vb.name = $vm_name
     vb.memory = $vm_memory
     vb.cpus = $vm_cpus
     vb.customize ["modifyvm", :id, "--ostype", "Linux_64"]
     vb.customize ["modifyvm", :id, "--hwvirtex", "on"]
     vb.customize ["modifyvm", :id, "--vtxvpid", "on"]
     vb.customize ["modifyvm", :id, "--vtxux", "on"]
     vb.customize ["modifyvm", :id, "--nestedpaging", "on"]
     vb.customize ["modifyvm", :id, "--pae", "on"]
     vb.customize ["modifyvm", :id, "--paravirtprovider", "kvm"]
     vb.customize ["modifyvm", :id, "--audio", "none"]
     if $vm_gui == true
        vb.gui = true
        vb.customize ["modifyvm", :id, "--vram", "128"]
        vb.customize ["modifyvm", :id, "--graphicscontroller", "vmsvga"]
        vb.customize ["modifyvm", :id, "--accelerate3d", "on"]
        vb.customize ["modifyvm", :id, "--mouse", "usbtablet"]
     else
        vb.gui = false
     end
  end

  if $vm_proxy == true
    if $vm_proxy_certificate != ''
      #if behind a enterprise proxy should be necessary to configure the certificate (root-CA)
      config.vm.provision :shell, path: "config-proxy-certificate.sh", args: [$vm_proxy_certificate]
    end
  end

  #udpate and installation of kernel libs
  config.vm.provision "shell", inline: <<-SHELL
    yum update -y && yum upgrade -y
    yum install -y epel-release
    yum install -y gcc dkms kernel-devel kernel-headers
    yum install -y wget
  SHELL

  #ssh config
  config.vm.provision :shell, path: "config-ssh.sh"

  #jdk installation
  config.vm.provision :shell, path: "setup-jdk.sh", args: [$jdk_version]

  if $vm_gui == true

    #MATE Desktop installation
    #https://mate-desktop.org/
    config.vm.provision "shell", inline: <<-SHELL
      yum groupinstall -y "MATE Desktop" "X Window System" "Fonts"
      systemctl isolate graphical.target
      systemctl set-default graphical.target
      systemctl start graphical.target
    SHELL

    #manual installation of virtualbox guest ddditions instead of using the vagrant-vbguest plugin
    #config.vm.provision "shell", inline: <<-SHELL
    #  wget http://download.virtualbox.org/virtualbox/6.0.4/VBoxGuestAdditions_6.0.4.iso
    #  mkdir /media/VBoxGuestAdditions
    #  mount -o loop,ro VBoxGuestAdditions_6.0.4.iso /media/VBoxGuestAdditions
    #  sh /media/VBoxGuestAdditions/VBoxLinuxAdditions.run
    #  rm -f VBoxGuestAdditions_6.0.4.iso
    #  umount /media/VBoxGuestAdditions
    #  rmdir /media/VBoxGuestAdditions
    #SHELL

  end

  config.vm.provision :reload

end
