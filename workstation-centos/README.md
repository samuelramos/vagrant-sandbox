workstation-ibm-esb
===================

## Pre-Requirements
-------------------

Before start you should have the following software installed in your workstation

- Vagrant 2.2.4
[download](https://www.vagrantup.com/downloads.html)
- VirtualBox 6.0.4
[download](https://www.virtualbox.org/wiki/Downloads)
- VirtualBox 6.0.4 Oracle VM VirtualBox Extension Pack
[download](https://www.virtualbox.org/wiki/Downloads)

Initiate your workstation-ibm-esb environment
---------------------------------------------

### 1) Initialization of the environment variables

If you're behind a enterprise proxy should be necessary to configure the proxy address

#### If you're using a mac

Open a new Terminal and run the following commands

```console
$ set HTTP_PROXY=http://address:port/
```
and
```console
$ set HTTPS_PROXY=https://address:port/
```
and
```console
$ set HOME=\Users\%USER%
```

#### If you're using a PC

Open a new Command Prompt and run the following commands

```console
# set HTTP_PROXY=http://address:port/
```
and
```console
# set HTTPS_PROXY=http://address:port/
```
and
```console
#set HOMEPATH=\Users\%USERNAME%
```

### 2) Configuration

You should modify the config.yaml file accordingly the environment you want to create in your local machine

### 3) Configure a enterprise proxy certificate in Vagrant

If you're behind a enterprise proxy it should be necessary to configure the ssl certificate in vagrant

#### If you're using a mac

1. Go to your Vagrant home directory (e.g. '/opt/vagrant/')
2. Enter into 'embedded' directory
3. Open and/or edit 'cacert.pem' as administrator
4. Open your enterprise proxy certificate, copy it's content and add it to the end of 'cacert.pem' file

#### If you're using a PC

1. Go to your Vagrant home directory (e.g. 'C:\HashiCorp\Vagrant\')
2. Enter into 'embedded' directory
3. Open and/or edit 'cacert.pem' as administrator
4. Open your enterprise proxy certificate, copy it's content and add it to the end of 'cacert.pem' file

### 4) Initialization and startup of the workstation-ibm-esb

In previous Terminal or Command Prompt (from step #1), navigate to the workstation-ibm-esb git directory and run the following command

```console
$ vagrant up
```

It will take a few minutes to complete, but, once the command finishes the VM is ready to use.

### 5) Using workstation-ibm-esb

1. To stop the workstation-ibm-esb you can use the operative system functionalities as usual, or use the VirtualBox
2. To start the workstation-ibm-esb again, you can use the VirtualBox

### 6) Manual configuration in workstation-ibm-esb

Once you logged in in your workstation-ibm-esb with user (vagrant) and password (vagrant) you will need to configure the following

#### Keyboard layout

1. In the upper menu bar go to 'System > Control Center' and select 'Keyboard'.
2. Select 'Layouts' tab.
3. Select 'Add'
4. On 'Country' dropdown select 'Portugual' and click 'Add'
5. Select the 'English (US)' from the list and click 'Remove'
6. Click 'Close'

#### Proxy script

If you're behind a enterprise proxy should be necessary to configure the proxy script

1. In the upper menu bar go to 'System > Control Center' and select 'Network Proxy'
2. Select 'Automatic proxy configuration'
3. In the textfield 'Autoconfiguration URL' add the following 'http://address/script.pac'
4. Click 'Close'

#### Firefox configuration

If you're behind a enterprise proxy, while opening the Firefox, you will be prompted for 'User Name' and 'Password'. You should use your enterprise credentials, namely, email and password.


### 7) Connect to a Git server

Before cloning a git repository to your VM, if you're behind a enterprise proxy it should be necessary to set proper configuration.

Through a system console, log as administrator using the command

```console
$ sudo -i
```

To disable proxy use

```console
$ git config --system --unset http.proxy
$ git config --system --unset https.proxy
```

To enable proxy use

```console
$ git config --system http.proxy http://address:port/
$ git config --system https.proxy https://address:port/
```
Additionally, if git server has a ssl certificate it should be necessary to configure ssl verification

To disable ssl use

```console
$ git config --system http.sslVerify false
$ git config --system https.sslVerify false
```

To enable ssl use

```console
$ git config --system http.sslVerify true
$ git config --system https.sslVerify true
```

Then, return to 'vagrant' user

```console
$ exit
```

Once you're done, navigate to the git directory and clone a git repository to your VM

```console
$ cd /home/vagrant/git-repo
$ git clone git@bitbucket.org:<username>/<git-repo>.git
```

Destroy your workstation-ibm-esb environment
--------------------------------------------

You can destroy the workstation-ibm-esb through VirtualBox or running the following command on the Terminal or Command Prompt you use to start it (from step #4)

```console
$ vagrant destroy
```

## Restore your workstation-ibm-esb environment
-------------------------------------------

1. Backup all your work that can be subject of loss and then
2. Destroy your workstation-ibm-esb environment
3. Initiate your workstation-ibm-esb environment

## Update your workstation-ibm-esb environment
------------------------------------------

1. Backup all your work that can be subject of loss and then
2. Pull the latest release from workstation-ibm-esb repository
3. Download the latest version of IBM software if any
3. Destroy your workstation-ibm-esb environment
4. Initiate your workstation-ibm-esb environment

## Know issues
--------------

### IBM Integration Toolkit on RedHat 6.5 crashes with "JVM terminated. Exit code=160" error (46681) -> Open

From <https://www-01.ibm.com/support/docview.wss?uid=swg27045067#knownproblems>

### Detected IIBM Integration BUS Problems -> Open

https://www-01.ibm.com/support/docview.wss?uid=swg27045067#knownproblems

IBM Integration Toolkit on Linux becomes unresponsive after opening a DFDL file or an XSD file (49065/60518)
From <https://www-01.ibm.com/support/docview.wss?uid=swg27045067#knownproblems>
